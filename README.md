# Twitter Feed React

## Installing Dependencies
* On OSX or Linux
    ```
    ./install.sh
    ```

* On Windows
1. Run `npm install` in this directory
2. Run `npm install` in the `api` directory
3. Run `npm install` in the `web` directory

## Configuring the site
1. Create a copy of the file `api/src/config.example.js` and name it `config.js`. Place it in the same directory as the example file.
2. Open the `config.js` file and replace all the example variables either with custom ones or by fetching the appropriate secrets/tokens from the Twitter developer website.

## Running the site
In either the `api` or `web` directories run the following command
```
npm start
```

To access the site, navigate to [http://localhost:8000](http://localhost:8000)

## Running tests and linter
In either the `api` or `web` directories run the following command
```
npm test
```

