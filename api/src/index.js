'use strict';

import express from 'express';
import cors from 'cors';
import config from './config';

import authenticateRoutes from './routes/twitter'; 

const app = express();

app.use(cors({
    origin: 'http://localhost:8000',
}));

app.use(authenticateRoutes);

app.listen(config.port, config.host, () => {
    console.log(`Listening on http://${config.host}:${config.port}`);
});