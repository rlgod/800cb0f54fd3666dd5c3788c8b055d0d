'use strict';

import config from '../config';
import nonce from 'nonce-generator';
import percentEncode from 'oauth-percent-encode';
import crypto from 'crypto';

export default class OAuthHeaders {
    constructor() {
        this.consumerKey = config.consumerKey;
        this.version = '1.0';
        this.signature_method = 'HMAC-SHA1';
        this.consumerSecret = config.consumerSecret;
    }

    // Creates a cryptographic signature of the baseString
    generateSignature(baseString, signingKey) {
        return crypto.createHmac('sha1', signingKey).update(baseString).digest('base64');
    }

    // Combines the 3 parts of the base string
    generateSignatureBaseString(method, url, parameterString) {
        return `${method.toUpperCase()}&${percentEncode(url)}&${percentEncode(parameterString)}`;
    }

    // Resolves the composite key used for signing the base string
    getSigningKey(requestTokenSecret) {
        return `${percentEncode(this.consumerSecret)}&${requestTokenSecret ? percentEncode(requestTokenSecret) : ''}`;
    }

    // Prepares the parameter string
    prepareParameterString(srcParameters) {
        const percentEncodedParameters = {};

        // Percent encode each of the parameters
        Object.keys(srcParameters).map((key) => {
            const val = srcParameters[key];
            // We coerce the key and value to strings first because the percentEncode
            // will only encode a string correctly and not booleans or numbers
            percentEncodedParameters[percentEncode(`${key}`)] = percentEncode(`${val}`);
        });

        // We only need to sort by keys because as per Twitter's documentation they don't accept duplicate keys anyway
        // so dong a secondary sort on the values is unnecessary
        const keys = Object.keys(percentEncodedParameters);

        keys.sort();

        // Build a string with the keys and values
        const parameterString = keys.reduce((parameterString, key, index) => {
            return `${parameterString}${index === 0 ? '' : '&'}${key}=${percentEncodedParameters[key]}`;
        }, '');

        return parameterString;
    }

    generateHeaderString(parameters) {
        return Object.keys(parameters).reduce((headerString, key, index) => {
            return `${headerString}${index === 0 ? '' : ', '}${percentEncode(`${key}`)}="${percentEncode(`${parameters[key]}`)}"`;
        }, 'OAuth ');
    }

    // Helper function to extract query params from url and return them in an array
    getQueryParams(url) {
        const splitUrl = url.split('?');

        if (splitUrl.length === 2) {
            // We have query params
            const queryParams = splitUrl[1].split('&');
            return queryParams.reduce((params, val) => {
                const key = val.split('=')[0];
                const value = decodeURIComponent(val.split('=')[1]);
                params[key] = value;
                return params;
            }, {});
        }

        return {};
    }

    // Function which takes raw inputs and generates the Authorization header with signature
    getHeader(url, method, requestTokenSecret, additionalParameters) {
        // We first collect all the parameters into an object
        const baseParameters = {
            oauth_consumer_key: this.consumerKey,
            oauth_nonce: nonce(32),
            oauth_signature_method: this.signature_method,
            oauth_timestamp: Math.floor(Date.now() / 1000),
            oauth_version: this.version,
        };

        const beforeSignParameters = Object.assign({}, baseParameters, additionalParameters);

        const queryParams = this.getQueryParams(url);

        const parameterString = this.prepareParameterString({
            ...beforeSignParameters,
            ...queryParams,
        });

        const baseUrl = url.split('?')[0];

        const signatureBaseString = this.generateSignatureBaseString(method, baseUrl, parameterString);

        const signingKey = this.getSigningKey(requestTokenSecret);

        const signature = this.generateSignature(signatureBaseString, signingKey);

        return this.generateHeaderString({
            ...beforeSignParameters,
            oauth_signature: signature,
        });
    }
}