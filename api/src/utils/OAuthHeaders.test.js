import OAuthHeaders from './OAuthHeaders';

jest.mock('nonce-generator', () => {
    return () => 'constant1nonce';
});

jest.mock('../config', () => {
    return {
        consumerKey: 'xvz1evFS4wEEPTGEFPHBog',
        consumerSecret: 'kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw',
    };
});

import config from '../config';

Date.now = jest.fn(() => '1111111111000');

const oAuthHeaders = new OAuthHeaders();

describe('OAuthHeaders prepareParameterString', () => {
    it('should generate the correct parameter string', () => {
        const result = oAuthHeaders.prepareParameterString({
            status: 'Hello Ladies + Gentlemen, a signed OAuth request!',
            include_entities: true,
            oauth_version: '1.0',
            oauth_consumer_key: config.consumerKey,
            oauth_signature_method: 'HMAC-SHA1',
            oauth_nonce: 'kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg',
            oauth_timestamp: "1318622958",
            oauth_token: '370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb',
        });
    
        expect(result).toBe(`include_entities=true&oauth_consumer_key=xvz1evFS4wEEPTGEFPHBog&oauth_nonce=kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1318622958&oauth_token=370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb&oauth_version=1.0&status=Hello%20Ladies%20%2B%20Gentlemen%2C%20a%20signed%20OAuth%20request%21`);
    });
});

describe('OAuthHeaders generateSignatureBaseString', () => {
    it('should generate the correct base string', () => {
        const baseUrl = 'https://api.twitter.com/1.1/statuses/update.json';
        const method = 'POST';
        const parameterString = `include_entities=true&oauth_consumer_key=xvz1evFS4wEEPTGEFPHBog&oauth_nonce=kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1318622958&oauth_token=370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb&oauth_version=1.0&status=Hello%20Ladies%20%2B%20Gentlemen%2C%20a%20signed%20OAuth%20request%21`;

        const result = oAuthHeaders.generateSignatureBaseString(method, baseUrl, parameterString);

        expect(result).toBe(`POST&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fupdate.json&include_entities%3Dtrue%26oauth_consumer_key%3Dxvz1evFS4wEEPTGEFPHBog%26oauth_nonce%3DkYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1318622958%26oauth_token%3D370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb%26oauth_version%3D1.0%26status%3DHello%2520Ladies%2520%252B%2520Gentlemen%252C%2520a%2520signed%2520OAuth%2520request%2521`);
    });
});

describe('OAuthHeaders getSigningKey', () => {
    it('should generate the correct signing key', () => {
        const result = oAuthHeaders.getSigningKey('LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE');

        expect(result).toBe('kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw&LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE');
    });
});

describe('OAuthHeaders getHeader()', () => {
    it('Should correctly generate a set of headers with request token', () => {
        const headers = oAuthHeaders.getHeader('http://localhost', 'POST', null, {
            my: 'param',
            oauth_token: 'request-token',
        });

        expect(headers).toBe('OAuth oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog", oauth_nonce="constant1nonce", oauth_signature_method="HMAC-SHA1", oauth_timestamp="1111111111", oauth_version="1.0", my="param", oauth_token="request-token", oauth_signature="XF5BX8xv14hX6uVQd5JlBSvW1ZQ%3D"');
    });

    it('Should correctly generate a set of headers with a full composite key', () => {
        const headers = oAuthHeaders.getHeader('http://localhost', 'POST', 'composite', {
            my: 'param',
            oauth_token: 'request-token',
        });

        expect(headers).toBe('OAuth oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog", oauth_nonce="constant1nonce", oauth_signature_method="HMAC-SHA1", oauth_timestamp="1111111111", oauth_version="1.0", my="param", oauth_token="request-token", oauth_signature="XNypI2%2BMBPeAI%2FZO%2FT1r62BzrmA%3D"');
    });

    it('Should correctly generate a set of headers without request token', () => {
        const headers = oAuthHeaders.getHeader('http://localhost', 'POST', null, {
            my: 'param',
        });

        expect(headers).toBe('OAuth oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog", oauth_nonce="constant1nonce", oauth_signature_method="HMAC-SHA1", oauth_timestamp="1111111111", oauth_version="1.0", my="param", oauth_signature="JQZBOw5S%2BRHxS%2BaJucHMSoX3KbQ%3D"');
    });

    it('Should produce the same result as the twitter docs', () => {
        // https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature

        // We can provide all the parameters in the additionalParams field 
        // because we're performing an Object.assign under the hood
        const headers = oAuthHeaders.getHeader('https://api.twitter.com/1.1/statuses/update.json?include_entities=true', 'POST', 'LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE', {
            status: 'Hello Ladies + Gentlemen, a signed OAuth request!',
            include_entities: true,
            oauth_consumer_key: 'xvz1evFS4wEEPTGEFPHBog',
            oauth_nonce: 'kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg',
            oauth_signature_method: 'HMAC-SHA1',
            oauth_timestamp: 1318622958,
            oauth_token: '370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb',
            oauth_version: '1.0',
        });

        expect(headers).toBe('OAuth oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog", oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg", oauth_signature_method="HMAC-SHA1", oauth_timestamp="1318622958", oauth_version="1.0", status="Hello%20Ladies%20%2B%20Gentlemen%2C%20a%20signed%20OAuth%20request%21", include_entities="true", oauth_token="370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb", oauth_signature="hCtSmYh%2BiHYCEqBWrE7C7hYmtUk%3D"');
    });
});