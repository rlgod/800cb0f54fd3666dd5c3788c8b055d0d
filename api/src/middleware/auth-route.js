import jwt from 'jsonwebtoken';
import config from '../config';

export default (req, res, next) => {
    const authorization = req.headers.authorization;

    const token = authorization.split('Bearer ')[1];
    try {
        const decoded = jwt.verify(token, config.jwtSecret);
        req.tokenPayload = decoded;
        next();
    } catch(err) {
        res.status(401, 'Unauthorized');
    }
};