'use strict';

import express from 'express';
import rp from 'request-promise';
import jwt from 'jsonwebtoken';
import config from '../config';
import OAuthHeaders from '../utils/OAuthHeaders';
import authRoute from '../middleware/auth-route';

const oAuthHeaders = new OAuthHeaders();
const Router = express.Router();

const tempStore = {};

// Helper for generating our own JSON Web Tokens
const generateAuthToken = (userId, screenName) => {
    return jwt.sign({
        screen_name: screenName,
    }, config.jwtSecret, {
        subject: userId,
        expiresIn: '1 hour',
    });
};

// Twitter token parser
const authTokenParser = (data) => {
    const parts = data.split('&');
    
    return parts.reduce((tokens, val) => {
        const keyValueSplit = val.split('=');
        tokens[keyValueSplit[0]] = keyValueSplit[1];
        return tokens;
    }, {});
};

Router.get('/oauth_request', (req, res) => {
    const callbackUrl = `http://${config.host}:${config.port}/connect`;

    const twitterRequestTokenEndpoint = 'https://api.twitter.com/oauth/request_token';
    const method = 'POST';

    const headerString = oAuthHeaders.getHeader(twitterRequestTokenEndpoint, method, null, {
        oauth_callback: callbackUrl,
    });

    rp({
        uri: twitterRequestTokenEndpoint,
        method: method,
        headers: {
            'Authorization': headerString,
        },
    }).then(data => {
        const authData = authTokenParser(data);

        if (authData.oauth_callback_confirmed) {
            tempStore[authData.oauth_token] = true;
            res.redirect(`https://api.twitter.com/oauth/authenticate?oauth_token=${authData.oauth_token}`);
        } else {
            res.status(500).send('We could not confirm the callback url');
        }
    });
});

Router.get('/connect', (req, res) => {
    const twitterConvertTokenEndpoint = 'https://api.twitter.com/oauth/access_token';
    const method = 'POST';

    if (!tempStore[req.query.oauth_token]) {
        return res.status(401).send('You are not authorized');
    }

    const headerString = oAuthHeaders.getHeader(twitterConvertTokenEndpoint, method, null, {
        oauth_token: req.query.oauth_token,
        oauth_verifier: req.query.oauth_verifier,
    });

    rp({
        uri: twitterConvertTokenEndpoint,
        method: method,
        headers: {
            'Authorization': headerString,
        },
    }).then(data => {
        const authData = authTokenParser(data);
        
        tempStore[authData.user_id] = authData;

        const token = generateAuthToken(authData.user_id, authData.screen_name);

        res.redirect(`${config.websiteUrl}/callback?token=${token}`);
    }).catch(() => {
        res.status(500).send('Something went very wrong');
    });
});

Router.post('/connect', authRoute, (req, res) => {
    const twitterVerifyCredentials = 'https://api.twitter.com/1.1/account/verify_credentials.json';
    const method = 'GET';

    if (!tempStore[req.tokenPayload.sub]) {
        return res.status(401).send('You are not authorized');
    }

    const userAuth = tempStore[req.tokenPayload.sub];

    const headerString = oAuthHeaders.getHeader(twitterVerifyCredentials, method, userAuth.oauth_token_secret, {
        oauth_token: userAuth.oauth_token,
    });

    rp({
        uri: twitterVerifyCredentials,
        method: method,
        headers: {
            'Authorization': headerString,
        },
    }).then(data => {
        return res.send(data);
    }).catch(err => {
        console.error(err);
        res.status(401).send('Failed to verify your credentials');
    });
});

Router.post('/disconnect', authRoute, (req, res) => {
    if (!tempStore[req.tokenPayload.sub]) {
        console.log('Already logged out');
        return res.send('You are logged out');
    }

    const authData = tempStore[req.tokenPayload.sub];

    delete tempStore[authData.oauth_token];
    delete tempStore[req.tokenPayload.sub];

    return res.send(authData.user_id);
});

Router.get('/tweets', authRoute, (req, res) => {
    const twitterFetchStatuses = `https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=${req.tokenPayload.sub}&count=100`;
    const method = 'GET';

    if (!tempStore[req.tokenPayload.sub]) {
        return res.status(401).send('You are not authorized');
    }

    const userAuth = tempStore[req.tokenPayload.sub];

    const headerString = oAuthHeaders.getHeader(twitterFetchStatuses, method, userAuth.oauth_token_secret, {
        oauth_token: userAuth.oauth_token,
    });

    rp({
        uri: twitterFetchStatuses,
        method: method,
        headers: {
            'Authorization': headerString,
        },
    }).then(data => {
        return res.send(data);
    }).catch(err => {
        console.error(err);
        return res.status(500).send('Something went wrong');
    });
});

export default Router;