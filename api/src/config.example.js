export default {
    host: 'localhost',
    port: 4000,
    websiteUrl: 'http://localhost:8000',
    consumerKey: 'Retrieve from twitter app token page',
    consumerSecret: 'Retrieve from twitter app token page',
    tokenSecret: 'Retrieve from twitter app token page',
    jwtSecret: 'secretsauce', // Not the most secure secret, but definitely the sauciest
};