import * as actions from '../actions/actionTypes';
import tweetReducers from './tweetReducers';

describe('authReducers', () => {
    it('should FETCH_TWEETS_FAILED', () => {
        const message = 'message';

        const state = tweetReducers.tweetReducers({}, {
            type: actions.FETCH_TWEETS_FAILED,
            message: message,
        });

        expect(state).toEqual({
            fetchTweetsFailedMessage: message,
        });
    });

    it('should FETCH_TWEETS_REQUESTED', () => {
        const state = tweetReducers.tweetReducers({}, {
            type: actions.FETCH_TWEETS_REQUESTED,
        });

        expect(state).toEqual({
            tweetsFetching: true,
        });
    });

    it('should FETCH_TWEETS_SUCCEEDED', () => {
        const tweets = [
            {text: "tweet"},
        ];

        const state = tweetReducers.tweetReducers({
            tweetsFetching: true,
        }, {
            type: actions.FETCH_TWEETS_SUCCEEDED,
            tweets: tweets,
        });

        expect(state).toEqual({
            tweets: tweets,
            tweetsFetching: false,
        });
    });

    it('should do nothing when any other action is passed', () => {
        const currentState = {};
        
        const state = tweetReducers.tweetReducers({}, {
            type: actions.LOGOUT_SUCCEEDED,
        });

        expect(state).toEqual(currentState);
    });
});