import AuthReducers from './authReducers';
import TweetReducers from './tweetReducers';
import ReduxPersistReducers from './reduxPersistReducers';

import { persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const config = {
    key: 'root',
    storage,
};

const reducers = persistCombineReducers(config, {
    ...AuthReducers,
    ...TweetReducers,
    ...ReduxPersistReducers,
});

export default reducers;