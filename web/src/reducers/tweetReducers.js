import * as actions from '../actions/actionTypes';

const tweetReducers = (state = {tweets: []}, action) => {
    switch(action.type) {
        case actions.FETCH_TWEETS_FAILED:
            return {
                ...state,
                fetchTweetsFailedMessage: action.message,
            };
        case actions.FETCH_TWEETS_REQUESTED:
            return {
                ...state,
                tweetsFetching: true,
            };
        case actions.FETCH_TWEETS_SUCCEEDED:
            return {
                ...state,
                tweets: action.tweets,
                tweetsFetching: false,
            };
        default:
            return state;
    }
};

export default {
    tweetReducers,
};