import _ from 'lodash';

import * as actions from '../actions/actionTypes';

const authReducers = (state = {}, action) => {
    switch(action.type) {
        case actions.SET_USER_TOKEN:
            return {
                ...state,
                token: action.token,
                loggedIn: true,
            };
        case actions.CLEAR_USER_TOKEN:
            return {
                ..._.omit(state, ['token']),
                loggedIn: false,
            };
        case actions.FETCH_PROFILE_REQUESTED:
            return {
                ...state,
                profileFetching: true,
            };
        case actions.FETCH_PROFILE_SUCCEEDED:
            return {
                ...state,
                profile: action.profile,
                profileFetching: false,
            };
        case actions.LOGOUT_SUCCEEDED:
            return {
                ...state,
                loggedIn: false,
            };
        default:
            return state;
    }
};

export default {
    authReducers,
};