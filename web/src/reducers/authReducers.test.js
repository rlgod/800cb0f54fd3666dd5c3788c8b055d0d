import * as actions from '../actions/actionTypes';
import authReducers from './authReducers';

describe('authReducers', () => {
    it('should SET_USER_TOKEN', () => {
        const token = 'token';
        const state = authReducers.authReducers({}, {
            type: actions.SET_USER_TOKEN,
            token: token,
        });

        expect(state).toEqual({
            token: token,
            loggedIn: true,
        });
    });

    it('should CLEAR_USER_TOKEN', () => {
        const state = authReducers.authReducers({
            loggedIn: true,
            token: 'token',
        }, {
            type: actions.CLEAR_USER_TOKEN,
        });

        expect(state).toEqual({
            loggedIn: false,
        });
    });

    it('should FETCH_PROFILE_REQUESTED', () => {
        const state = authReducers.authReducers({}, {
            type: actions.FETCH_PROFILE_REQUESTED,
        });

        expect(state).toEqual({
            profileFetching: true,
        });
    });

    it('should FETCH_PROFILE_SUCCEEDED', () => {
        const profile = {
            name: 'foo',
        };

        const state = authReducers.authReducers({}, {
            type: actions.FETCH_PROFILE_SUCCEEDED,
            profile: profile,
        });

        expect(state).toEqual({
            profileFetching: false,
            profile: profile,
        });
    });

    it('should LOGOUT_SUCCEEDED', () => {
        const state = authReducers.authReducers({}, {
            type: actions.LOGOUT_SUCCEEDED,
        });

        expect(state).toEqual({
            loggedIn: false,
        });
    });

    it('should do nothing when any other action is passed', () => {
        const currentState = {};
        
        const state = authReducers.authReducers({}, {
            type: actions.FETCH_TWEETS_REQUESTED,
        });

        expect(state).toEqual(currentState);
    });
});