import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { fetchTweets } from '../actions';

import Tweet from './Tweet';

const bottomMarginStyle = {
    marginBottom: '5px',
};

class Feed extends Component {
    static propTypes = {
        tweets: PropTypes.arrayOf(PropTypes.object),
        onFetchTweets: PropTypes.func,
        isFetching: PropTypes.bool,
    };

    constructor(props) {
        super(props);

        this.refreshFeed = this.refreshFeed.bind(this);
    }

    componentDidMount() {
        this.props.onFetchTweets();
    }

    refreshFeed() {
        this.props.onFetchTweets();
    }

    render() {
        const { tweets, isFetching } = this.props;

        const TweetViews = tweets.map((tweet) => {
            return <Tweet key={tweet.id} tweet={tweet}/>;
        });

        const loadingSpinner = isFetching ? <img className="loading-spinner" src="/images/gif-load.gif"/> : null;

        return (
            <div>
                <Button onClick={this.refreshFeed} style={bottomMarginStyle}>Refresh Tweets</Button>
                {loadingSpinner}
                {TweetViews}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        tweets: state.tweetReducers.tweets,
        isFetching: state.tweetReducers.tweetsFetching,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchTweets: () => {
            dispatch(fetchTweets());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Feed);