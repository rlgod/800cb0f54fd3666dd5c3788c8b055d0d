import React, { Component } from 'react';
import config from '../config';

export default class TwitterButton extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const oAuthRequestUrl = `${config.apiBaseUrl}/oauth_request`;

        return(
            <div>
                <div>
                    <p>Log in with Twitter to see your feed</p>
                </div>
                <div>
                    <a href={oAuthRequestUrl} className="mouse-pointer">
                        <img src="images/sign-in-with-twitter-gray.png"></img>
                    </a>
                </div>
            </div>
        );
    }
}