import React, { Component } from 'react';
import PropTypes from 'prop-types';

const style = {
	whiteSpace: 'pre-wrap',
};

export default class ErrorBoundary extends Component {
	static propTypes = {
		children: PropTypes.object,
	};

	constructor(props) {
		super(props);
		this.state = { error: false, errorInfo: null };
	}

	componentDidCatch(error, info) {
		this.setState({
			error: error,
			errorInfo: info,
		});
	}

	render() {
		if (this.state.errorInfo) {
			return (
				<div>
					<h2>Something went wrong.</h2>
					<details style={style}>
						{this.state.error && this.state.error.toString()}
						<br />
						{this.state.errorInfo.componentStack}
					</details>
				</div>
			);
		}

		return this.props.children;
	}
}