import { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { setUserToken } from '../actions';
import PropTypes from 'prop-types';

class CallbackComponent extends Component {
    static propTypes = {
        location: PropTypes.object,
        onUserTokenSet: PropTypes.func,
        history: PropTypes.object,
    };

    constructor(props) {
        super(props);

        const params = new URLSearchParams(props.location.search);

        this.token = params.get('token');

        localStorage.setItem('user_token', this.token);

        const { onUserTokenSet } = this.props;
        onUserTokenSet(this.token);
        
        this.props.history.push('/');
    }

    render() {
        return null;
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUserTokenSet: token => {
            dispatch(setUserToken(token));
        },
    };
};

export default connect(null, mapDispatchToProps)(withRouter(CallbackComponent));