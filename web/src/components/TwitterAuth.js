import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Feed from './Feed';
import Profile from './Profile';
import Logout from './Logout';
import TwitterButton from './TwitterButton';

class TwitterAuth extends Component {
    static propTypes = {
        loggedIn: PropTypes.bool,
    };

    constructor(props) {
        super(props);
    }

    render() {
        const { loggedIn } = this.props;

        if (!loggedIn) {
            return (
                <div>
                    <TwitterButton/>
                </div>
            );
        }

        return (
            <div>
                <Profile/>
                <Logout/>
                <Feed/>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loggedIn: state.authReducers.loggedIn,
    };
};

export default connect(mapStateToProps)(TwitterAuth);