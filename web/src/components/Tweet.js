import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Col } from 'react-bootstrap';

class Tweet extends Component {
    static propTypes = {
        tweet: PropTypes.object,
    };

    constructor(props) {
        super(props);
        this.tweetTextToHtml = this.tweetTextToHtml.bind(this);
    }

    tweetTextToHtml(text, entities, extended_entities) {
        text = `<p>${text}</p>`;

        // Mentions
        if (entities.user_mentions) {
            entities.user_mentions.map((mention) => {
                text = text.replace(`@${mention.screen_name}`, `<a href="https://twitter.com/${mention.screen_name}">@${mention.screen_name}</a>`);
            });
        }

        if (entities.hashtags) {
            entities.hashtags.map((hashtag) => {
                text = text.replace(`#${hashtag.text}`, `<a href="https://twitter.com/hastag/${hashtag.text}">#${hashtag.text}</a>`);
            });
        }
        
        if (entities.urls) {
            entities.urls.map((url) => {
                text = text.replace(`${url.url}`, `<a href="${url.expanded_url}">${url.url}</a>`);
            });    
        }
        
        if (entities.media) {
            entities.media.map((media) => {
                const extendedMedia = _.find(extended_entities.media, {id: media.id});
                if (extendedMedia.type === 'video') {
                    text = `${text}<video class="tweet-media" poster="${extendedMedia.media_url_https}" controls>
                        <source type="${extendedMedia.video_info.variants[0].content_type}" src="${extendedMedia.video_info.variants[0].url}">
                    </video>`;
                } else {
                    text = `${text}<a href="${media.expanded_url}"><img class="tweet-media" src="${media.media_url_https}"/></a>`;
                }
            });
        }

        return {__html: text};
    }

    render() {
        let { tweet } = this.props;
        let wasRetweet = null;

        const profileUrl = `https://twitter.com/${tweet.user.screen_name}`;

        if (tweet.retweeted_status) {
            tweet = tweet.retweeted_status;
            wasRetweet = (
                <Col xs={12} className="retweeted">
                    <p>You Retweeted</p>
                </Col>
            );
                
        }

        const textHtml = this.tweetTextToHtml(tweet.text, tweet.entities, tweet.extended_entities);
        const tweetLink = `${profileUrl}/status/${tweet.id_str}`;

        return (
            <div className="tweet-card">
                {wasRetweet}
                <Col xs={2} lg={1}>
                    <img className="image-circle" src={tweet.user.profile_image_url_https}/>
                </Col>
                <Col xs={10} lg={11} className="tweet-body">
                    <Col xs={12}>
                        <a href={profileUrl}>{tweet.user.name}</a> <span className="handle">@{tweet.user.screen_name}</span>
                    </Col>
                    <Col xs={12}>
                        <div dangerouslySetInnerHTML={textHtml}></div>
                    </Col>
                </Col>
                <Col className="margin-top-standard" xs={12}>
                    <p><a href={tweetLink} target="_blank">View on Twitter</a></p>
                </Col>
            </div>
        );
    }
}

export default Tweet;