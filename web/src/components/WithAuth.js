import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { setUserToken, clearUserToken, checkUserToken } from '../actions';

class WithAuth extends Component {
    static propTypes = {
        onCheckUserToken: PropTypes.func,
        onUserTokenClear: PropTypes.func,
        onUserTokenSet: PropTypes.func,
        children: PropTypes.object,
    };

    constructor(props) {
        super(props);
        
    }

    componentDidMount() {
        this.props.onCheckUserToken();
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCheckUserToken: () => {
            dispatch(checkUserToken());
        },
        onUserTokenSet: token => {
            dispatch(setUserToken(token));
        },
        onUserTokenClear: () => {
            dispatch(clearUserToken());
        },
    };
};

export default connect(null, mapDispatchToProps)(WithAuth);