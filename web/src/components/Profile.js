import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchProfile } from '../actions';

class Profile extends Component {
    static propTypes = {
        onFetchProfile: PropTypes.func,
        profile: PropTypes.object,
    };

    constructor(props) {
        super(props);
    }
    
    componentDidMount() {
        this.props.onFetchProfile();
    }    

    render() {
        const { name, screen_name, profile_image_url_https } = this.props.profile;

        const twitterProfileLink = `https://twitter.com/${screen_name}`;

        return (
            <div className="profile">
                <Col xs={2} lg={1}>
                    <a href={twitterProfileLink} target="_blank">
                        <img className="image-circle" src={profile_image_url_https}/>
                    </a>
                </Col>
                <Col xs={10} lg={11}>
                    <Col xs={12}>
                        <a href={twitterProfileLink} target="_blank">{name}</a>
                    </Col>
                    <Col xs={12}>
                        <span className="handle">@{screen_name}</span>
                    </Col>
                </Col>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        profile: state.authReducers.profile || {},
        isLoading: state.authReducers.profileFetching,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchProfile: () => {
            dispatch(fetchProfile());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);