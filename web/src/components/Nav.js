import 'babel-polyfill';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Col } from 'react-bootstrap';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import { PersistGate } from 'redux-persist/lib/integration/react';

import ErrorBoundary from './ErrorBoundary';
import TwitterAuth from './TwitterAuth';
import WithAuth from './WithAuth';
import CallbackComponent from './CallbackComponent';

import reducers from '../reducers';
import sagas from '../sagas';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    reducers,
    undefined,
    compose(
        applyMiddleware(sagaMiddleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    ),
);

const persistor = persistStore(store);

sagaMiddleware.run(sagas);

export default class nav extends Component {
    static propTypes = {
        title: PropTypes.string,
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate persistor={persistor}>
                    <ErrorBoundary>
                        <Router>
                            <Col xs={12} sm={8} smOffset={2} md={6} mdOffset={3}>
                                <h1>{this.props.title}</h1>
                                
                                <Route path="/" render={() => 
                                    <WithAuth>
                                        <TwitterAuth/>
                                    </WithAuth>
                                }/>
                                <Route path="/callback" component={CallbackComponent}/>
                            </Col>
                        </Router>
                    </ErrorBoundary>
                </PersistGate>
            </Provider>
        );
    } 
}