import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import { logout } from '../actions';
import PropTypes from 'prop-types';

const floatRightStyle = {
    float: 'right',
};

class Logout extends Component {
    static propTypes = {
        doLogout: PropTypes.func,
    };

    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout() {
        this.props.doLogout();
    }

    render() {
        return (
            <Button onClick={this.logout} style={floatRightStyle}>Logout</Button>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        doLogout: () => {
            dispatch(logout());
        },
    };
};

export default connect(null, mapDispatchToProps)(Logout);