import { call, put, takeLatest, fork } from 'redux-saga/effects';
import Api from '../libs/api';
import jwt from 'jsonwebtoken';

import { 
    fetchTweetsSucceeded,
    fetchTweetsFailed,
    fetchProfileSucceeded,
    fetchProfileFailed,
    logoutSucceeded,
    clearUserToken,
    setUserToken,
} from '../actions';

import {
    FETCH_TWEETS_REQUESTED,
    FETCH_PROFILE_REQUESTED,
    LOGOUT_REQUESTED,
    CHECK_USER_TOKEN,
} from '../actions/actionTypes';

function* fetchTweets() {
    try {
        const response = yield call(Api.fetchTweets);
        yield put(fetchTweetsSucceeded(response.data));
    } catch (e) {
        yield put(fetchTweetsFailed(e.message));
    }
}

function* fetchProfile() {
    try {
        const response = yield call(Api.fetchProfile);
        yield put(fetchProfileSucceeded(response.data));
    } catch (e) {
        yield put(fetchProfileFailed(e.message));
    }
}

function* checkUserToken() {
    const lsToken = localStorage.getItem('user_token');
    if (lsToken) {
        const payload = jwt.decode(lsToken);
        if (payload.exp < (Date.now() / 1000)) {
            // token has expired. Kick them out.
            yield put(clearUserToken());
            localStorage.removeItem('user_token');
        } else {
            yield put(setUserToken(lsToken));
        }
    } else {
        yield put(clearUserToken());
    }
}

function* logout() {
    try {
        yield call(Api.logout);
        localStorage.removeItem('user_token');
        yield put(logoutSucceeded());
    } catch (e) {
        // We invoke succeeded anyway so we can log them out locally
        yield put(logoutSucceeded());
    }
}

function* tweetSaga() {
    yield takeLatest(FETCH_TWEETS_REQUESTED, fetchTweets);
}

function* profileSaga() {
    yield takeLatest(FETCH_PROFILE_REQUESTED, fetchProfile);
}

function* logoutSaga() {
    yield takeLatest(LOGOUT_REQUESTED, logout);
}

function* checkUserTokenSaga() {
    yield takeLatest(CHECK_USER_TOKEN, checkUserToken);
}

export default function* root() {
    yield [
        fork(tweetSaga),
        fork(profileSaga),
        fork(logoutSaga),
        fork(checkUserTokenSaga),
    ];
}