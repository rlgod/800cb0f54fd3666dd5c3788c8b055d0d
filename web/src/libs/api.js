import axios from 'axios';
import config from '../config';

const getToken = () => {
    return localStorage.getItem('user_token');
};

const getAuthToken = () => {
    return axios({
        method: 'GET',
        url: `${config.apiBaseUrl}/oauth_request`,
    });
};

const fetchTweets = () => {
    return axios({
        method: 'GET',
        url: `${config.apiBaseUrl}/tweets`,
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    });
};

const fetchProfile = () => {
    return axios({
        method: 'POST',
        url: `${config.apiBaseUrl}/connect`,
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    });
};

const logout = () => {
    return axios({
        method: 'POST',
        url: `${config.apiBaseUrl}/disconnect`,
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    });
};

const Api = {
    getAuthToken,
    fetchTweets,
    fetchProfile,
    logout,
};

export default Api;