import * as actions from './actionTypes';

const setUserToken = (token) => {
    return {
        type: actions.SET_USER_TOKEN,
        token: token,
    };
};

const clearUserToken = () => {
    return {
        type: actions.CLEAR_USER_TOKEN,
    };
};

const fetchTweets = () => {
    return {
        type: actions.FETCH_TWEETS_REQUESTED,
    };
};

const fetchTweetsSucceeded = (tweets) => {
    return { 
        type: actions.FETCH_TWEETS_SUCCEEDED,
        tweets: tweets,
    };
};

const fetchTweetsFailed = (message) => {
    return { 
        type: actions.FETCH_TWEETS_FAILED,
         message: message,
    };
};

const fetchProfile = () => {
    return {
        type: actions.FETCH_PROFILE_REQUESTED,
    };
};

const fetchProfileSucceeded = (profile) => {
    return {
        type: actions.FETCH_PROFILE_SUCCEEDED,
        profile: profile,
    };
};

const fetchProfileFailed = (message) => {
    return {
        type: actions.FETCH_PROFILE_FAILED,
        message: message,
    };
};

const checkUserToken = () => {
    return {
        type: actions.CHECK_USER_TOKEN,
    };
};

const logout = () => {
    return {
        type: actions.LOGOUT_REQUESTED,
    };
};

const logoutSucceeded = () => {
    return {
        type: actions.LOGOUT_SUCCEEDED,
    };
};

export {
    checkUserToken,
    setUserToken,
    clearUserToken,

    fetchTweets,
    fetchTweetsSucceeded,
    fetchTweetsFailed,

    fetchProfile,
    fetchProfileSucceeded,
    fetchProfileFailed,

    logout,
    logoutSucceeded,
};