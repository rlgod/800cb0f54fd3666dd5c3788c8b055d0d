import React from 'react';
import { render } from 'react-dom';
import Nav from './components/Nav';

window.onload = () => {
  var root = document.createElement('div');
  root.id = 'react-root';

  document.body.appendChild(root);

  render(<Nav title="Twitter Feed"/>, root);
};