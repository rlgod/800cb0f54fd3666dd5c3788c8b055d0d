const gulp = require('gulp');

const path = require('path');

const browserify = require('browserify'),
    babelify = require('babelify'),
    buffer = require('vinyl-buffer'),
    source = require('vinyl-source-stream'),
    sourcemaps = require('gulp-sourcemaps'),
    pathmodify = require('pathmodify'),
    watchify = require('watchify'),
    notifier = require('node-notifier'),
    webserver = require('gulp-webserver');

const BUILD_DIR = './build';

function compile(watch) {
    let bundler = browserify({
        entries: ['./src/app.js'],
        debug: true,
        plugin: !watch ? []: [ watchify ],
        cache: {},
        packageCache: {},
    });

    function bundle() {
        return bundler
            .plugin(pathmodify, {
                mods: [function(rec) {
                    if (rec.id[0] === '/' && !rec.id.startsWith(__dirname)) { //eslint-disable-line
                        return { id: path.join(__dirname, rec.id.substr(1)) }; //eslint-disable-line
                    }

                    return {};
                }],
            })
            .transform(babelify, { presets: ['es2015', 'react', 'stage-0']})
            .bundle()
            .on('error', function(err) {
                console.log(err.stack); //eslint-disable-line
                notifier.notify({
                    title: 'Build Error',
                    message: err.message,
                });
                this.emit('end');
            })
            .pipe(source('app.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init({ loadMaps: true }))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(BUILD_DIR));
    }

    if (watch) {
        bundler = watchify(bundler)
            .on('update', bundle);
    }

    return bundle();
}

gulp.task('copy:images', function() {
    return gulp.src(['src/images/**/*.*'])
        .pipe(gulp.dest(`${BUILD_DIR}/images`));
});

gulp.task('copy:css', function() {
    return gulp.src(['src/styles/*.css'])
        .pipe(gulp.dest(`${BUILD_DIR}/styles`));
});

gulp.task('build:js', function () {
    return compile(false);
});

gulp.task('build:html', function() {
    return gulp.src(['src/**/*.html', '!node_modules/**/*'])
        .pipe(gulp.dest(BUILD_DIR));
});

gulp.task('build', ['build:js', 'build:html', 'copy:images', 'copy:css']);

gulp.task('serve', function() {
  return gulp.src(BUILD_DIR)
    .pipe(webserver({
        host: 'localhost',
        livereload: true,
        fallback: 'index.html',
    }));
});

gulp.task('watch:css', function() {
    return gulp.watch(['src/**/*.css'], ['copy:css']);
});

gulp.task('watch:js', function() {
    return gulp.watch(['src/**/*.js'], ['build:js']);
});

gulp.task('watch:html', function() {
    return gulp.watch(['src/**/*.html'], ['build:html']);
});

gulp.task('watch', ['build', 'serve', 'watch:js', 'watch:html', 'watch:css']);

gulp.task('default', ['build', 'serve']);
